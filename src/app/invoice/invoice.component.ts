import { Component, OnInit, Output, EventEmitter } from '@angular/core';
 import {Invoice} from './invoice'

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Invoice>();
 @Output() editEvent = new EventEmitter<Invoice>();
  invoice:Invoice;
  isEdit : boolean = false;
  editButtonText = 'Edit';
  

  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.invoice);
  }
  

  
 
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit'
    if(!this.isEdit){
      this.editEvent.emit(this.invoice);
    }
  }
  ngOnInit() {
  }

}
