import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Customer} from '../customer/customer'

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

   @Output() customerAddedEvent = new EventEmitter<Customer>();
  customer:Customer = {
    name: '',
    age: ''
  };

  constructor() { }

  onSubmit(form:NgForm){

    console.log(form);
    this.customerAddedEvent.emit(this.customer);
    this.customer = {
       name: '',
       age: ''
    }
  }

  ngOnInit() {
  }

}
