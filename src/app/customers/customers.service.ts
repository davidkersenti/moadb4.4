import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class CustomersService {
  private _url = "http://davidke.myweb.jce.ac.il/api/customers/";

 constructor(private af:AngularFire, private _http:Http ) { }

 getCustomerFromRestApi(){
    let url = this._url;
    return this._http.get(url).map(res => res.json()).delay(2000)
  }

}
