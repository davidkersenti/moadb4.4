import { Component, OnInit } from '@angular/core';
import {CustomersService} from './customers.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styles: [`
    .customers li { cursor: default; }
    .customers li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class CustomersComponent implements OnInit {
  customers;
  currentCustomer;

  isLoading = true;

  constructor(private _customersService: CustomersService) {
    //this.users = this._userService.getUsers();
  }
  select(customer){
		this.currentCustomer = this.customers[1]; 
    console.log(	this.currentCustomer);
 }
 addCustomer(customer){
    this.customers.push(customer)
  }
 deleteCustomer(customer){
    this.customers.splice(
      this.customers.indexOf(customer),1
    )
  }
  

  ngOnInit() {
        this._customersService.getCustomerFromRestApi()
			    .subscribe(customers => {this.customers = customers.result;
                               this.isLoading = false;
                               console.log(this.customers)});
  }


}
