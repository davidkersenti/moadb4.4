import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Customer} from './customer'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  inputs:['customer']
})
export class CustomerComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Customer>();
  customer:Customer;
  isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.customer);
  }
  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';    

  }

  ngOnInit() {
  }

}
